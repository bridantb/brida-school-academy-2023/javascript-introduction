---
title: Class Introduction
titleTemplate: "%s - BSA 2023"
layout: cover
---

<h1 class="font-semibold">BRIDA School Academy</h1>
<div class="flex flex-col gap-20">
  <div class="text-3xl font-medium">📚 Class Introduction</div>
  <div class="font-medium">Agung Dwitatwa A.</div>
</div>

---

## layout: cover

<div class="font-semibold text-2xl mt-2"> 📖 Berkenalan Dengan Dunia Pemrograman</div>
<div class="text-base mt-5">Apa yang perlu dikuasai ?</div>

```ts
  1. Pemikiran Logis
  2. Matematika Dasar
  3. Rasa Ingin Tahu dan Kesabaran
```

---

<h1 class="font-semibold">Pemikiran Logis</h1>
<div class="text-base mt-5">1. Memecahkan Masalah Sederhana</div>

```ts
  Jika sebuah apel berharga 10.000 rupiah dan kamu membeli 3 apel,
  berapa total uang yang harus kamu bayar?
```

<div class="text-base mt-10">Bagaimana cara menyelesaikan permasalahan ini ?</div>

```ts{1|2,3|5|6,7|9|10|12|13}
  Identifikasi data yang diberikan :
  Harga apel = 10.000 rupiah
  jumlah apel yang dibeli = 3

  Tentukan operasi yang diperlukan:
  Kita perlu mengalikan harga apel dengan jumlah apel
  yang dibeli untuk mendapatkan total uang yang harus dibayarkan.

  Hitung hasilnya:
  10.000 rupiah (harga) x 3 (jumlah) = 30.000 rupiah.

  Jawab pertanyaan:
  Total uang yang harus dibayarkan adalah 30.000 rupiah.
```

---
