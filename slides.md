---
title: Introduction
titleTemplate: "%s - BSA 2023"
layout: cover
presenter: true
export:
  dark: false
---

<h1 class="font-semibold">BRIDA School Academy</h1>
<div class="flex flex-col gap-20">
  <div class="text-3xl font-medium">📚 Introduction</div>
  <div class="font-medium">Agung Dwitatwa A.</div>
</div>

---
layout: cover
title: Mindmap
---

```mermaid
  
  flowchart LR
  A["Introduction"]
  B["Introduction to Programming"]
  C["Introduction to Javascript"]
  D["Introduction to Typescript"]
  E["Typescript Vs Javascript"]
    

  A --> B
  B --> C
  B --> D
  B --> E

```

---
layout: cover
title: Pemrograman adalah
---
<h1 class="font-semibold">Apa itu Pemrograman?</h1>
<div class="w-2xl">
  <span class="bg-yellow-300 dark:text-black">Pemrograman</span>
  adalah seni memberikan <span class="bg-yellow-300 dark:text-black">instruksi kepada komputer</span> untuk melakukan tugas.
  Ibarat kita mengajari bahasa pada mesin
</div>

---
layout: cover
title: Berkenalan Dengan Dunia Pemrograman
---

<div class="font-semibold text-2xl mt-2"> 📖 Berkenalan dengan dunia pemrograman</div>
<div class="text-base mt-5">Apa yang diperlukan ?</div>

```ts{1|2|3}
  1. Pemikiran Logis
  2. Matematika Dasar
  3. Rasa Ingin Tahu dan Kesabaran
```

---
title: Pemikiran Logis 01
---
<h1 class="font-semibold">Contoh Pemikiran Logis</h1>
<div class="text-base mt-5">1. Memecahkan Masalah Sederhana</div>

```ts
  Jika sebuah apel berharga 10.000 rupiah dan kamu membeli 3 apel,
  berapa total uang yang harus kamu bayar?
```
<v-clicks>
<div class="text-base mt-5 font-semibold">Bagaimana cara menyelesaikan permasalahan ini ?</div>

<div class="text-sm mt-4">Identifikasi data yang diberikan :</div>

```ts{1|all}
  Harga apel = 10.000 rupiah
  jumlah apel yang dibeli = 3
```
<div class="text-sm mt-4">Tentukan operasi yang diperlukan :</div>

```ts
  Kita perlu mengalikan harga apel dengan jumlah apel
  yang dibeli untuk mendapatkan total uang yang harus dibayarkan.
```
<div class="text-sm mt-4">Hitung hasilnya :</div>

```ts
  10.000 rupiah (harga) x 3 (jumlah) = 30.000 rupiah.
```
</v-clicks>

---
title: Pemikiran Logis 02
---
<h1 class="font-semibold">Contoh Pemikiran Logis</h1>
<div class="text-base mt-5">2. Pengambilan Keputusan</div>

```ts
  Misalkan ada situasi berikut: 
  "Jika hari ini hujan, saya akan membawa payung. Jika tidak, saya tidak membawa payung."
```
<v-clicks>
<div class="text-base mt-5 font-semibold">Bagaimana cara menyelesaikan permasalahan ini ?</div>

<div class="text-sm mt-4">Identifikasi kondisi: </div>

```ts
  Kondisi adalah apakah hari ini hujan atau tidak.
```
<div class="text-sm mt-4">Tentukan tindakan yang diperlukan : </div>

```ts
  Jika hujan, bawa payung. Jika tidak, tidak bawa payung.
```
<div class="text-sm mt-4">Lakukan pengambilan keputusan :</div>

```ts
  Lihat cuaca saat ini. Jika hujan, ambil payung. 
  Jika tidak hujan, tidak perlu membawa payung.
```

</v-clicks>

---
title: Pemikiran Logis 03
---
<h1 class="font-semibold">Contoh Pemikiran Logis</h1>
<div class="text-base mt-5">3. Memecahkan Masalah Lebih Kompleks</div>

```ts
  Seorang petani memiliki ladang dengan luas 2000 meter persegi. 
  Dia ingin membagi ladang tersebut menjadi 4 bagian dengan luas yang sama.
  Berapa luas masing-masing bagian?
```
<v-clicks>
<div class="text-base mt-5 font-semibold">Bagaimana cara menyelesaikan permasalahan ini ?</div>

<div class="text-sm mt-4">Identifikasi data yang diberikan: </div>

```ts{1|all}
  Luas ladang = 2000 meter persegi 
  jumlah bagian = 4.
```

<div class="text-sm mt-4">Tentukan operasi yang diperlukan: </div>

```ts
  Kita perlu membagi luas ladang dengan jumlah bagian 
  untuk mendapatkan luas masing-masing bagian.
```
<div class="text-sm mt-4">Hitung hasilnya: </div>

```ts
  2000 meter persegi (luas) / 4 (jumlah bagian) = 500 meter persegi.
```

</v-clicks>

---
layout: cover
title : Pertanyaan alur instruksi
---
<div class="text-4xl font-semibold">Bagaimana cara <span class="bg-yellow-300">mentransfer</span> pikiran logis tadi ke komputer ?</div>

---
title: Alur Instruksi?
---

<div class="ml-2 text-3xl font-semibold mb-3">Alur Instruksi ?</div>
<v-clicks>

```mermaid
  flowchart LR
  A["Manusia"]
  B["Instruksi"]
  C["Komputer"]
  D["？"]

  A --> B
  B --> C
  C --> D

  style D fill:#fff,stroke:#fff
```
```mermaid
  flowchart LR
  A["Manusia"]
  B["Instruksi"]
  C["Komputer"]
  D["🚫 Tidak Bisa"]

  A --> B
  B --> C
  C --> D

  style D fill:#fff,stroke:#fff  
```
```mermaid
  flowchart LR
  A["Manusia"]
  B["Instruksi"]
  C["Bahasa Pemrograman"]
  D["Komputer"]
  E["？"]

  A --> B
  B --> C
  C --> D
  D --> E

  style E fill:#fff,stroke:#fff
```
```mermaid
  flowchart LR
  A["Manusia"]
  B["Instruksi"]
  C["Bahasa Pemrograman"]
  D["Komputer"]
  E["🚫 Tidak Bisa"]

  A --> B
  B --> C
  C --> D
  D --> E

  style E fill:#fff,stroke:#fff  
```
```mermaid
 flowchart LR
  A["Manusia"]
  B["Instruksi"]
  C["Bahasa Pemrograman"]
  D["Binary Code"]
  E["Komputer"]
  F["✅ Bisa"]

  A --> B
  B --> C
  C --> D
  D --> E
  E --> F

  style F fill:#fff,stroke:#fff
```
</v-clicks>
---
title: Bahasa Pemrograman
---

<img src="/programming-language.jpeg" class="w-full" />


---
title: Meme Programming Language Level
---
<img src="/meme-level-programming-language.jpg" class="h-[450px] mx-auto" />

---
layout: cover
title: Introduction to Javascript
---
<div class="text-5xl font-semibold mb-0">Introduction to Javascript</div>

<div class="w-3xl mt-4 leading-8">
  <span class="bg-yellow-300">Bahasa pemrograman Javascript</span> merupakan bahasa pemrograman tingkat tinggi yang tadinya hanya digunakan untuk membuat website menjadi <span class="bg-yellow-300">lebih interaktif</span>. Dengan adanya runtime <span class="bg-yellow-300">NodeJS</span> membuat Javascript bisa digunakan untuk membuat <span class="bg-yellow-300">aplikasi Cross Platform</span>.
</div>

---
layout: cover
title: Konfigurasi Environtment
---

<div class="text-3xl font-semibold bg-yellow-300 w-fit">Konfigurasi Environtment</div>
<div class="mt-4 leading-8">
  Konfigurasi Environtment bertujuan untuk mempersiapkan <span class="bg-yellow-300">lingkungan pada server</span> dengan berbagai tools, supaya bahasa pemrograman <span class="bg-yellow-300">bisa dimengerti oleh komputer.</span>
</div>

---
layout: two-cols
title: Konfigurasi Runtime
---

<div class="font-semibold">Konfigurasi Runtime</div>
<div class="mt-3 text-sm pr-3">NodeJS adalah runtime yang digunakan untuk menjalankan bahasa pemrograman Javascript di server</div>
<img src="/nodejs.png" class="w-[400px] border mt-3" />


::right::

<div>Link Download</div>

```ts
https://nodejs.org/en/download
```
<img src="/NodejsDownloadPage.png" class="w-full mt-3 rounded-lg">

---
title: Konfigurasi Code Editor
---
<div class="font-semibold">Konfigurasi Code Editor</div>
<div class="mt-3 text-sm">Code Editor ini digunakan buat menulis script code yang nantinya akan dilakukan konversi oleh NodeJS menjadi binary code supaya bisa berjalan di komputer.</div>

```ts
https://code.visualstudio.com/
```
  <img src="/vscodeInstallPage.png" class="w-[600px]" />
---
layout: cover
title: Javascript Basics
---
<div class="text-3xl font-semibold bg-yellow-300 w-fit">Javascript Basics</div>
<div class="mt-4 leading-8">
  Hal - hal yang perlu diketahui dalam bahasa pemrograman menggunakan Javascript.
</div>


---
title: Learn The Basics
layout: two-cols
---
<div class="font-semibold text-xl">Learn The Basics</div>
<div class="text-lg mt-2 mb-10">
  Pada dasarnya pemrograman itu adalah <span class="bg-yellow-400">membuat skenario</span> yang akan dilewati selama aplikasi atau sistem itu dijalankan. Semakin kompleks sekenario yang dibuat, semakin banyak permasalahan yang di selesaikan.
</div>

```mermaid
  flowchart LR
  A["Skenario"]
  B["Algoritma"]

  A-->B
```

<div class="text-lg mt-10">
  Untuk memudahkan pemahaman algoritma oleh komputer, bahasa pemrograman menyediakan berbagai fitur yang dapat digunakan sesuai kebutuhan
</div>

::right::

<div class="p-10">
  <img src="https://i.pinimg.com/564x/d3/0e/15/d30e15ff38f3cece90b0937f98333961.jpg" class="w-full pl-10">
</div>

---
title: Fitur Javascript
layout: cover
---

```mermaid { scale: 1.1}
  flowchart LR
  A["Javascript"]
  B["Variables"]
  C["Operators"]
  D["Control Statements"]
  E["Function"]
  G["... etc"]
  F["Basic Features"]

  A --> F
  F --> B
  F --> C
  F --> D
  F --> E
  F --> G
```

---
title: Javascript Variables
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Variables</div>

<div class="text-base font-medium bg-yellow-300 w-fit">Cara Penggunaan:</div>

```ts
const contoh = 10;
let Contoh = 20;
```

<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Aturan Penamaan</div>
<ol class="text-sm">
  <li>Harus dimulai dengan huruf, garis bawah (_), atau tanda dolar ($).</li>
  <li>Tidak dapat dimulai dengan angka.</li>
  <li>Dapat berisi huruf, angka, garis bawah, atau tanda dolar.</li>
  <li>Bersifat case-sensitive.</li>
</ol>

<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Tips penggunaan</div>
<ol class="text-sm">
  <li>Untuk variabel yang terdiri dari beberapa kata, gunakan camel case: jumlahTotalHutang, namaDepan.
  </li>
  <li>Gunakan nama yang mencerminkan tujuan variabel. Daripada a atau x, gunakan namaPengguna atau jumlahTotal.
  </li>
  <li>
    Selalu mulai dengan const. Jika ada kemungkinan variable nilainya perlu diubah, baru gunakan let.
  </li>
</ol>

---
title: Javascript Primitive Data Types
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Variable Data Types</div>

<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Tipe Data Primitif :</div>
<ol class="text-sm">
  <li>Number</li>
  <li>String</li>
  <li>Boolean</li>
  <li>Undefined</li>
  <li>Null</li>
</ol>

---
title: Contoh Penggunaan Variable
layout: cover
---

<div class="text-xl font-medium">Skenario :</div>
<div class="text-sm">Kamu bekerja di sebuah toko buku dan mendapatkan komisi dari setiap penjualan buku. Untuk setiap buku jenis A yang terjual, kamu mendapatkan komisi sebesar Rp5.000, dan untuk setiap buku jenis B yang terjual, kamu mendapatkan komisi sebesar Rp7.000. </div>

<div class="text-lg font-medium mt-4">Kondisi :</div>
<ol class="list-decimal text-sm">
<li>Jumlah buku jenis A yang terjual: 10</li>
<li>Jumlah buku jenis B yang terjual: 5</li>
</ol>

<div class="text-lg font-medium mt-4">Masalah :</div>
<div class="text-sm">Kamu ingin mengetahui berapa total komisi yang kamu dapatkan hari ini.</div>

---
title: Javascript Non Primitive Data Types
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Variable Data Types</div>

<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Tipe Data Non-Primitif :</div>
<ol class="text-sm">
  <li>Object</li>
  <li>Array</li>
  <li>Function</li>
</ol>

---
title: Array
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Variable Data Types</div>

<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Array :</div>

```ts
Array adalah kumpulan dari beberapa nilai yang disimpan dalam satu variabel. 
Bayangkan Anda memiliki kotak sepatu, dan dalam kotak tersebut ada beberapa sepatu. 
Array bekerja dengan cara yang sama, memungkinkan Anda menyimpan banyak nilai dalam satu variabel.
```

<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Cara Penggunaan</div>

```ts
let buku = [value1, value2, value3];
```
<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Contoh Penggunaan</div>

```ts
let buku = ["Fiksi", "Non-Fiksi", "Biografi"];
console.log(buku[0]); // Hasilnya: "Fiksi"

buku[1] = "Ensiklopedia";
console.log(buku); // Hasilnya: ["Fiksi", "Ensiklopedia", "Biografi"]
console.log(buku.length); // Hasilnya: 3
buku.push("Komik");
console.log(buku); // Hasilnya: ["Fiksi", "Non-Fiksi", "Biografi","Komik"]
buku.pop();
console.log(buku); // Hasilnya: ["Fiksi", "Non-Fiksi", "Biografi"]
```

---
title: Object
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Variable Data Types</div>
<div class="pr-4 flex items-start gap-5">
  <div>
    <div class="text-base font-medium bg-yellow-300 w-fit">Object :</div>

  ```ts
  Object adalah kumpulan dari pasangan kunci-nilai. Jika Kita menganggap 
  array seperti kotak sepatu yang berisi sepatu dengan nomor urut, maka 
  objek mirip dengan lemari arsip, di mana setiap dokumen memiliki label 
  dan isinya.
  ```
  </div>

  <div>
    <div class="text-base font-medium bg-yellow-300 w-fit">Cara Penggunaan :</div>

  ```ts
  let mahasiswa = {
      key1: value1,
      key2: value2,
  };
  ```
  </div>
</div>

<div>
  <div class="text-base font-medium bg-yellow-300 w-fit mt-4">Contoh Penggunaan</div>

  ```ts
  let mahasiswa = {
      nama: "Budi",
      umur: 20,
      jurusan: "Teknik Informatika"
  };

  console.log(mahasiswa.nama); // Hasilnya: "Budi"
  mahasiswa.umur = 21;
  console.log(mahasiswa.umur); // Hasilnya: 21
  mahasiswa.jurusan = "Teknik Informatika";
  console.log(mahasiswa.jurusan); // Hasilnya: "Teknik Informatika"
  delete mahasiswa.umur;
  console.log(mahasiswa); // Hasilnya: {nama:"Budi",jurusan:"Teknik Informatika"}
  ```
</div>

---
title: Javascript Operator Aritmatika
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Operators</div>
<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Operator Aritmatika</div>

<table class="text-xs mt-2"><thead class="bg-slate-200"><tr><th>Operator</th><th>Deskripsi</th><th>Contoh Kode</th><th>Hasil</th></tr></thead><tbody><tr><td><code>+</code></td><td>Penjumlahan</td><td><code>5 + 3</code></td><td><code>8</code></td></tr><tr><td><code>-</code></td><td>Pengurangan</td><td><code>5 - 3</code></td><td><code>2</code></td></tr><tr><td><code>*</code></td><td>Perkalian</td><td><code>5 * 3</code></td><td><code>15</code></td></tr><tr><td><code>/</code></td><td>Pembagian</td><td><code>8 / 2</code></td><td><code>4</code></td></tr><tr><td><code>%</code></td><td>Modulus (sisa pembagian)</td><td><code>8 % 3</code></td><td><code>2</code></td></tr><tr><td><code>++</code></td><td>Inkrementasi</td><td><code>num = 5; num++</code></td><td><code>num = 6</code></td></tr><tr><td><code>--</code></td><td>Dekrementasi</td><td><code>num = 5; num--</code></td><td><code>num = 4</code></td></tr></tbody></table>

---
title: Javascript Operator Perbandingan
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Operators</div>
<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Operator Perbandingan</div>

<table class="mt-2 text-xs"><thead class="bg-slate-200"><tr><th>Operator</th><th>Deskripsi</th><th>Contoh Kode</th><th>Hasil</th></tr></thead><tbody><tr><td><code>==</code></td><td>Sama dengan (nilai)</td><td><code>5 == "5"</code></td><td><code>true</code></td></tr><tr><td><code>===</code></td><td>Identik (nilai dan tipe data)</td><td><code>5 === "5"</code></td><td><code>false</code></td></tr><tr><td><code>&lt;</code></td><td>Kurang dari</td><td><code>5 &lt; 10</code></td><td><code>true</code></td></tr><tr><td><code>&gt;</code></td><td>Lebih dari</td><td><code>10 &gt; 5</code></td><td><code>true</code></td></tr><tr><td><code>&lt;=</code></td><td>Kurang dari atau sama dengan</td><td><code>5 &lt;= 10</code></td><td><code>true</code></td></tr><tr><td><code>&gt;=</code></td><td>Lebih dari atau sama dengan</td><td><code>10 &gt;= 5</code></td><td><code>true</code></td></tr></tbody></table>

---
title: Javascript Operator Logika
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Operators</div>
<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Operator Logika</div>

<table class="mt-2 text-xs"><thead class="bg-slate-200"><tr><th>Operator</th><th>Deskripsi</th><th>Contoh Kode</th><th>Hasil</th></tr></thead><tbody><tr><td><code>&amp;&amp;</code></td><td>DAN</td><td><code>(5 &gt; 3) &amp;&amp; (6 &gt; 4)</code></td><td><code>true</code></td></tr><tr><td><code>||</code></td><td>ATAU</td><td><code>(5 &gt; 3) || (6 &lt; 4)</code></td><td><code>true</code></td></tr><tr><td><code>!</code></td><td>TIDAK</td><td><code>!(5 &gt; 10)</code></td><td><code>true</code></td></tr></tbody></table>



---
title: Javascript Statement Controls
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Statement Controls (if/else)</div>

<div class="text-base font-medium bg-yellow-300 w-fit">Cara Penggunaan:</div>

```ts
let umur = 18;

if (umur >= 17) {
    console.log("Boleh memilih di pemilu.");
} else {
    console.log("Belum boleh memilih.");
}
```

---
title: Javascript Looping
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Statement Controls (looping)</div>

<div class="text-base font-medium bg-yellow-300 w-fit">Cara Penggunaan:</div>

```ts
while (kondisi) {
  // kode yang dijalankan berulang-ulang
}

for (inisialisasi; kondisi; pembaruan) {
  // kode yang dijalankan berulang-ulang
}


```
<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Contoh :</div>

```ts
let i = 0;
while (i < 5) {
  console.log(i);
  i++;
}

for (let i = 0; i < 5; i++) {
  console.log(i); // Ini akan mencetak angka dari 0 hingga 4
}
```

---
title: Contoh Statement Control
layout: cover
---

<div class="text-xl font-medium">Skenario :</div>
<div class="text-sm">Si Aji punya toko buku di pasar buku kota B. Buku yang paling laris namanya "Rahasia Alam Semesta". Harganya Rp50.000 per buku. Biasanya, ada banyak guru atau orang tua yang beli buku ini dalam jumlah banyak buat anak atau muridnya. Jadi, Aji punya ide untuk memberikan insentif buat yang beli banyak. Kalau seseorang beli lebih dari 10 buku "Rahasia Alam Semesta", Aji kasih diskon 10%. Sekarang, Aji mau bikin program biar bisa otomatis ngitung total yang harus dibayar setelah diskon.</div>

<div class="text-lg font-medium mt-4">Kondisi :</div>
<div class="text-sm">Jumlah buku yang terjual sebanyak 14</div>

<div class="text-lg font-medium mt-4">Masalah :</div>
<div class="text-sm">Berapa total pembayaran setelah diskon</div>

---
title: Contoh Statement Control
layout: cover
---

<div class="text-xl font-medium">Skenario :</div>
<div class="text-sm">Anda ingin mengetahui apakah angka yang ada dalam suatu array adalah bilangan genap atau ganjil. Anda akan mencetak "Genap" untuk setiap angka genap dan "Ganjil" untuk setiap angka ganjil.</div>

<div class="text-lg font-medium mt-4">Kondisi :</div>
<div class="text-sm">array terdiri dari angka 1,2,3,4, dan 5</div>

<div class="text-lg font-medium mt-4">Masalah :</div>
<div class="text-sm">Menentukan angka yang termasuk bilangan ganjil dan genap pada array</div>

---
title: Javascript Function
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Function</div>

<div class="text-base font-medium bg-yellow-300 w-fit">Cara Penggunaan:</div>

```ts
function namaFungsi() {
    // kode yang akan dijalankan
}
```
<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Contoh Penggunaan:</div>

```ts
function sapa() {
    console.log("Halo, selamat datang!");
}

sapa();  // Menampilkan: "Halo, selamat datang!"
```

---
title: Javascript Function
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Function</div>

<div class="text-base font-medium bg-yellow-300 w-fit">Argumen dan Parameter</div>

```ts
Parameter adalah variabel yang digunakan saat mendeklarasikan fungsi. 
Parameter memungkinkan kita memberikan input ke fungsi saat memanggilnya.

Argumen adalah nilai yang kita berikan ke fungsi saat memanggilnya.
```
<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Contoh Penggunaan:</div>

```ts
function tambah(angka1, angka2) {
    console.log(angka1 + angka2);
}

tambah(5, 3);  // Menampilkan: 8

Di sini, angka1 dan angka2 adalah parameter, sedangkan 5 dan 3 adalah argumen.

```

---
title: Javascript Function
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Function</div>

<div class="text-base font-medium bg-yellow-300 w-fit">Nilai Kembalian</div>

```ts
Fungsi dapat mengembalikan nilai menggunakan kata kunci return.
```
<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Contoh Penggunaan:</div>

```ts
function kali(angka1, angka2) {
    return angka1 * angka2;
}

let hasil = kali(4, 5);
console.log(hasil);  // Menampilkan: 20
```

---
title: Javascript Function
layout: cover
---

<div class="font-medium mb-5 text-xl underline">Variable & Function</div>

<div class="text-base font-medium bg-yellow-300 w-fit">Tipe Tipe Fungsi</div>
<ol class="text-sm">
  <li>Anonymous Function</li>
  <li>Arrow Function</li>
</ol>


<div class="text-base font-medium bg-yellow-300 w-fit mt-4">Contoh Penggunaan:</div>

```ts
const inifungsi = function(){
  console.log("ini anonymous function");
}
inifungsi();

const inifungsi = () => console.log('ini Arrow function');
inifungsi();

const inifungsi = () => { 
  console.log('ini Arrow function')
};
inifungsi();

```

---
title: Contoh Function
layout: cover
---

<div class="text-xl font-medium">Skenario :</div>
<div class="text-sm">Dalam kehidupan sehari-hari, kita sering perlu melakukan perhitungan matematika sederhana seperti penambahan, pengurangan, perkalian, dan pembagian. Untuk memudahkan tugas ini, Anda memutuskan untuk membuat kalkulator sederhana dengan fungsi.</div>

<div class="text-lg font-medium mt-4">Kondisi :</div>
<div class="text-sm">Buat fungsi untuk masing masing operasi</div>

<div class="text-lg font-medium mt-4">Masalah :</div>
<div class="text-sm">Membuat program kalkulator sederhana</div>

---
title: Kahoot Time!
layout: center
---

<h1>Kahoot Time!</h1>

---
layout: cover
title: Typescript Introduction
---
<div class="text-3xl font-semibold bg-yellow-300 w-fit">Introduction to Typescript</div>
<div class="mt-4 leading-8">
  TypeScript adalah bahasa pemrograman open-source yang dikembangkan oleh Microsoft. TypeScript adalah superset dari JavaScript, yang berarti setiap kode JavaScript yang valid juga merupakan kode TypeScript yang valid.
</div>

---
layout: cover
title: Type Definition
---
<div class="text-3xl font-semibold bg-yellow-300 w-fit">Type Definition</div>
<ol class="text-sm mt-3">
  <li>Type Alias</li>
  <li>Type Union</li>
  <li>Type Intersection</li>
  <li>Type Literal</li>
</ol>

---
layout: cover
title: Contoh Type Definition
---
<div class="text-3xl font-semibold bg-yellow-300 w-fit">Type Definition</div>
<div class="grid grid-cols-2 gap-4 mt-4">
  <div>
    <div class="mt-2">Type Alias</div>

  ```ts
  type tipeUmur = number;
  let umur:tipeUmur = 8
  ```
  </div>

  <div>
    <div class="mt-2">Type Union</div>

  ```ts
  type tipeNilai = number | string;
  let nilai: tipeNilai = 42;
  nilai = "Empat puluh dua";

  ```
  </div>

  <div>
    <div class="mt-2">Type Intersection</div>

  ```ts
  
  type tipeNama = string;
  type tipeUsia = string | number;
  type Info = { nama: tipeNama } & { usia: tipeUsia };
  const orang = {
    nama:"Albus",
    usia:45
  }
  ```
  </div>

  <div>
    <div class="mt-2">Type Literals</div>

  ```ts
  type tipeTombolAksi = "KLIK" | "SENTUH" | "TAHAN";
  let tombolAkis:tipeTombolAksi = "KLIK";
  ```
  </div>
  
</div>

---
layout: cover
title: Deklarasi Variabel
---
<div class="text-3xl font-semibold bg-yellow-300 w-fit">Perbedaan Deklarasi Variables</div>
<div class="flex gap-5 mt-4">
  <div class="flex-1">
    <div class="bg-yellow-300">Javascript</div>
  
  ```ts
  let nama = "Budi";
  let umur = 25;
  const negara = "Indonesia";

  let daftarAngka = [1, 2, 3];

  let object = {
    nama:"Albud Dumbledore",
    umur:43
  }
  ```
  </div>
  <div class="flex-1">
    <div class="bg-blue-300">Typescript</div>
    
  ```ts
  let nama:string = "Budi";
  let umur:number = 25;
  const negara:string = "Indonesia";

  let daftarAngka: number[] = [1, 2, 3];

  let object:{nama:string,umur:number} = {
    nama:"Albud Dumbledore",
    umur:43
  }

  ```
  </div>
</div>

---
layout: cover
title: Deklarasi Function
---
<div class="text-3xl font-semibold bg-yellow-300 w-fit">Perbedaan Deklarasi Fungsi</div>
<div class="flex gap-5 mt-4">
  <div class="flex-1">
    <div class="bg-yellow-300">Javascript</div>
  
  ```ts
  function sapa(nama) {
    return "Halo, " + nama;
  }
  ```
  </div>
  <div class="flex-1">
    <div class="bg-blue-300">Typescript</div>
    
  ```ts
  function sapa(nama: string): string {
      return "Halo, " + nama;
  }
  ```
  </div>
</div>

---
layout: cover
title: Deklarasi Function
---
<div class="text-3xl font-semibold bg-yellow-300 w-fit">Javascript Vs Typescript</div>

<div class="flex gap-3 mt-4">
  <img src="/ts.png" class="w-[400px]" />
  <img src="/js.png" class="w-[400px]"  />
</div>

